#### Composició

La composició consisteix en la inclusió d'un objecte d'un cert tipus com
a atribut d'una classe. Per exemple:

```java
public class Cotxe() {
    private Motor motor;
    //...
}
```

Tant la composició com l'herència permeten reutilitzar altres objectes
dins d'una nova classe. ***La composició s'utilitza generalment quan es
volen les capacitats d'una classe existent dins de la nova classe, però
no es vol la seva interfície***. És a dir, s'inclou un objecte de manera
que es pot utilitzar per implementar noves característiques, però
l'usuari de la nova classe veu la interfície que s'ha definit allà, i no
la de l'objecte inclòs.

Un dels principis del disseny orientat objecte assegura que cal
“***afavorir la composició d'objectes per davant de l'herència de
classes***”.
