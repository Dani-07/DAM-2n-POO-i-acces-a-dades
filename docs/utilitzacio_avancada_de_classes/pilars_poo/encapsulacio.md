#### Encapsulació

L'encapsulació consisteix a amagar els detalls d'implementació d'una
classe als objectes que en fan ús. L'encapsulació és el mecanisme bàsic
que ens permet complir amb un dels principis fonamentals del bon disseny
orientat a objectes: **separar aquelles coses que varien d'aquelles que
són constants**.

L'usuari d'una biblioteca, és a dir, el programador que utilitza les
classes d'aquesta biblioteca, ha de poder confiar en la seva
funcionalitat, i en què el seu programa seguirà funcionant encara que
surti una nova versió de la biblioteca. Això vol dir que la interfície
de la biblioteca ha de ser constant i accessible a l'usuari, mentre que
la implementació interna i els objectes auxiliars que s'utilitzin han
d'estar ocults i, per tant, poder variar sense afectar al programa
client.

En termes d'implementació, els mètodes i constants que formen part de la
interfície d'una classe o d'una biblioteca hauran de ser públics, mentre
que els mètodes i variables utilitzats internament seran, en general,
privats.

Ja vam estudiar els modificadors *public*, *private*, (*default*) i
*protected* que ens ofereix el Java per triar el nivell d'ocultació que
volem aplicar a les dades i als mètodes d'una classe. A continuació
veurem com podem utilitzar les classes genèriques que hem creat en un
projecte en altres projectes.

- - -

**Exemple d'encapsulació**

[Codi d'exemple d'encapsulació](codi/utilitzacio_avancada_de_classes/exemple_encapsulacio)
