### Herència

 * [A què té accés una subclasse](acces.md)
 * [Crida de constructors](constructors.md)
 * [Sobreescriptura de mètodes](sobreescriptura.md)
 * [Exercicis](exercicis.md)
 
