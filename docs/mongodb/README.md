**POO i accés a dades**

M6UF3. persistència en BD natives XML
=====================================

Introducció i instal·lació
--------------------------

- [Introducció a MongoDB](introduccio.md)
- [Format JSON](json.md)
- [Introducció a Gradle](gradle.md)
- [Representació de documents](document.md)

Inserció de documents
---------------------

- [Establiment d'una connexió](connexio.md)
- [Inserció de documents](insercio.md)

Consultes bàsiques
------------------

- [Consultes](consultes.md)
- [Filtres](filtres.md)
- [Projeccions](projeccions.md)
- [Ordenació i límits](sort_limit.md)

Modificació i eliminació
------------------------

- [Modificació de dades](modificacio.md)
- [Eliminació de documents](eliminacio.md)

Agregacions
-----------

- [Aggregation framework](aggregation.md)

Exercicis
---------

- [Exercicis](exercicis.md)
