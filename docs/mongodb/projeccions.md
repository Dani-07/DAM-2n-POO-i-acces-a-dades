## Projeccions

Sovint no volem obtenir totes les dades d'un document quan fem una consulta,
sinó algunes dades concretes. Amb les projeccions podem indicar-li al SGBD
quines són les dades que necessitem.

L'ús de projeccions també pot accelerar la resposta a la nostra consulta. Si,
per exemple, tots els camps que indiquem a una projecció es poden obtenir
a través de índexos, serà molt més ràpid recuperar només aquests camps que
no pas documents sencers.

En els següents exemples treballem amb una col·lecció de punts en tres
dimensions. Cada punt té unes coordenades aleatòries:

```java
MongoClient client = new MongoClient();
MongoDatabase db = client.getDatabase("exemples");
MongoCollection<Document> coll = db.getCollection("punts");

coll.drop();

for (int i=0; i<100; i++) {
	int x = ThreadLocalRandom.current().nextInt(100);
	int y = ThreadLocalRandom.current().nextInt(100);
	int z = ThreadLocalRandom.current().nextInt(10);
	Document document = new Document("x",x).append("y", y).append("z", z);
	coll.insertOne(document);
}
```

En la creació d'aquesta col·lecció hem utilitzat el mètode
*ThreadLocalRandom.current()* que ens dóna accés a un generador de nombres
aleatoris propi del fil d'execució actual. En ocasions resulta més
còmode generar nombres aleatoris d'aquesta manera en comptes d'haver de
crear un objecte *Random*.

A la següent consulta recuperem els punts que tenen una coordenada *x* més
gran que 50 i una coordenada *z* igual a 5.

Mitjançant una projecció indiquem que ens interessen totes les dades, excepte
la *x* i l'*_id*.

```java
Bson filter = and(gt("x", 50), eq("z", 5));
// Retorna tots els elements, excepte que els s'indiqui amb un 0.
Bson projection = new Document("x", 0).append("_id", 0);
coll.find(filter).projection(projection).forEach(Document doc -> System.out.println(doc.toJson()));
System.out.println();
```

De nou, hem utilitzat els *import* estàtics de *Filters* per millorar la
llegibilitat de la consulta.

Com es pot veure, per fer la projecció n'hi ha prou amb crear un document
indicant els camps que volem (que no volem en aquest cas concret) i passar-lo
al mètode *projection()*.

La consulta equivalent a la terminal de MongoDB és:

```
db.punts.find({$and : [{"x" : {$gt:50}}, {"z" : {$eq:5}}]}, {"x":0,"_id":0})
```

També podem fer la projecció indicant quins camps volem en comptes de quins
camps no volem.

Per defecte, l'*_id* sempre es retorna, així que no cal indicar que el volem.

```java
// Retorna només els elements indicats amb un 1, i l'_id.
projection = new Document("x",1);
coll.find(filter).projection(projection).forEach(Document doc -> System.out.println(doc.toJson()));
System.out.println();
```

La consulta equivalent a la terminal és:

```
db.punts.find({$and : [{"x" : {$gt:50}}, {"z" : {$eq:5}}]}, {"x":1})
```

Si en el cas anterior no volem recuperar l'*_id* ho podem fer de la següent
manera:

```java
// Retorna només els elements indicats amb un 1.
projection = new Document("y", 1).append("_id", 0);
coll.find(filter).projection(projection).forEach(Document doc -> System.out.println(doc.toJson()));
System.out.println();
```

Aquesta consulta és equivalent a
`db.punts.find({$and : [{"x" : {$gt:50}}, {"z" : {$eq:5}}]}, {"y":1,"_id":0})`.

També existeix la classe *Projections* que ens permet escriure el mateix
tipus de projeccions sense haver de crear documents.

Aquesta consulta és equivalent al primer exemple:

```java
projection = Projections.exclude("x","_id");
coll.find(filter).projection(projection).forEach(Document doc -> System.out.println(doc.toJson()));
System.out.println();
```

Aquesta és equivalent al segon exemple:

```java
projection = Projections.include("x");
coll.find(filter).projection(projection).forEach(Document doc -> System.out.println(doc.toJson()));
System.out.println();
```

I aquesta al tercer:

```java
projection = Projections.fields(Projections.include("y"), Projections.excludeId());
coll.find(filter).projection(projection).forEach(Document doc -> System.out.println(doc.toJson()));
System.out.println();
```

La classe *Projections* és una bona candidata per utilitzar els *import static*
tal com ho hem fet amb *Filters*.

Pots consultar el
[codi complet](codi/mongodb/src/main/java/mongodb/ExempleProjection.java) de
l'exemple.
