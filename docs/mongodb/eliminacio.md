## Eliminació de documents

Veiem ara com es poden eliminar documents. Ja hem vist que el mètode
*drop()* ens permet eliminar una col·lecció sencera.

A més, disposem dels mètodes *deleteOne()* i *deleteMany()* per esborrar un
o diversos documents respectivament.

Recreem la col·lecció de punts amb 25 punts bidimensionals:

```java
MongoClient client = new MongoClient();
MongoDatabase db = client.getDatabase("exemples");
MongoCollection<Document> coll = db.getCollection("punts");

coll.drop();

for (int x=0; x<5; x++) {
	for (int y=0; y<5; y++) {
		coll.insertOne(new Document("x",x).append("y", y));
	}
}
```

Eliminem ara tots els punts que tenen la coordenada *x* més gran que 2. A
continuació, esborrem el primer document que té un 1 com a coordenada *y*:

```java
// esborra tots els elements que compleixen la condició
coll.deleteMany(gt("x", 2));
// esborra el primer element que compleix la condició
coll.deleteOne(eq("y", 1));
```

Mostrem a continuació quins són els punts que s'han mantingut:

```java
List<Document> all = coll.find().into(new ArrayList<Document>());
for (Document doc : all) {
	System.out.println(doc.toJson());
}
System.out.println();
```

El resultat és el següent:

```
{ "_id" : { "$oid" : "56af9e8cec24140ddc2503ec" }, "x" : 0, "y" : 0 }
{ "_id" : { "$oid" : "56af9e8cec24140ddc2503ee" }, "x" : 0, "y" : 2 }
{ "_id" : { "$oid" : "56af9e8cec24140ddc2503ef" }, "x" : 0, "y" : 3 }
{ "_id" : { "$oid" : "56af9e8cec24140ddc2503f0" }, "x" : 0, "y" : 4 }
{ "_id" : { "$oid" : "56af9e8cec24140ddc2503f1" }, "x" : 1, "y" : 0 }
{ "_id" : { "$oid" : "56af9e8cec24140ddc2503f2" }, "x" : 1, "y" : 1 }
{ "_id" : { "$oid" : "56af9e8cec24140ddc2503f3" }, "x" : 1, "y" : 2 }
{ "_id" : { "$oid" : "56af9e8cec24140ddc2503f4" }, "x" : 1, "y" : 3 }
{ "_id" : { "$oid" : "56af9e8cec24140ddc2503f5" }, "x" : 1, "y" : 4 }
{ "_id" : { "$oid" : "56af9e8cec24140ddc2503f6" }, "x" : 2, "y" : 0 }
{ "_id" : { "$oid" : "56af9e8cec24140ddc2503f7" }, "x" : 2, "y" : 1 }
{ "_id" : { "$oid" : "56af9e8cec24140ddc2503f8" }, "x" : 2, "y" : 2 }
{ "_id" : { "$oid" : "56af9e8cec24140ddc2503f9" }, "x" : 2, "y" : 3 }
{ "_id" : { "$oid" : "56af9e8cec24140ddc2503fa" }, "x" : 2, "y" : 4 }
```

Podem comprovar com han desaparegut tots els punts amb *x* igual a 3 o 4 i com
també ha desaparegut el punt *(0,1)*.

Pots consultar el
[codi complet](codi/mongodb/src/main/java/mongodb/ExempleEsborrat.java) de
l'exemple.
