## Consultes

A continuació veurem com podem recuperar els documents que hem guardat a
una col·lecció.

Com passa amb la sentència *SELECT* de SQL, les consultes en MongoDB
accepten molts modificadors que permeten especificar exactament què volem
recuperar.

En aquest apartat ens centrarem en consultes que només necessiten recuperar
documents d'una col·lecció.

En el següent exemple creem una col·lecció de punts. Per a cada punt guardem
les seves coordenades.

A continuació utilitzem una colla de mètodes habituals per obtenir els
punts guardats:

```java
public class ExempleFind {

	public static void main(String[] args) {
		MongoClient client = new MongoClient();
		MongoDatabase db = client.getDatabase("exemples");
		MongoCollection<Document> collection = db.getCollection("punts");

		collection.drop();

		for (int x=0; x<5; x++) {
			for (int y=0; y<5; y++) {
				collection.insertOne(new Document("x",x).append("y", y));
			}
		}

		// Obtenir el primer element
		Document first = collection.find().first();
		System.out.println(first.toJson());
		System.out.println();
		// Recuperar tots els elements en una List
		List<Document> all = collection.find().into(new ArrayList<Document>());
		for (Document doc : all) {
			System.out.println(doc.toJson());
		}
		System.out.println();
		// Iterar per tots els elements sense guardar-los
		try (MongoCursor<Document> cursor = collection.find().iterator()) {
			while (cursor.hasNext()) {
				Document doc = cursor.next();
				System.out.println(doc.toJson());
			}
		}
		System.out.println();
		// Iterar per tots els elements, estil Java 8
		collection.find().forEach((Document doc) -> System.out.println(doc.toJson()));
		System.out.println();

		// Comptar la quantitat d'elements
		System.out.println(collection.count());

		client.close();
	}

}
```

Quan recuperem documents d'una col·lecció, l'ordre més habitual és *find()*.
A l'exemple podem comprovar que hi ha dues formes principals d'utilitzar el
*find()*: guardar tots els elements recuperats a una llista, o bé utilitzar
un cursor per recórrer-los.

L'expressió
`List<Document> all = collection.find().into(new ArrayList<Document>());`
guarda tots els elements recuperats a la llista que li passem per paràmetre.

Com que *into()* ens retorna la mateixa llista, podem fer com a l'exemple:
cridar el constructor de la llista directament quan cridem el mètode i
recuperar-la després ja plena quan el mètode retorna.

Aquest sistema és útil qual la quantitat de dades a recuperar no és molt gran
o quan necessitem tenir-les totes guardades en memòria per un temps llarg.

Si la quantitat de documents obtinguts és molt gran, aquest mètode pot ser
lent i pot necessitar molta memòria. En aquest cas és millor utilitzar un
cursor.

A l'exemple hem vist dues maneres d'utilitzar un cursor: amb
`collection.find().iterator()` o amb `collection.find().forEach()`.

Independentment de quin sistema utilitzem, un cursor ens retorna un dels
documents recuperats per iteració, de manera que els podem anar processant
un a un sense necessitat de tenir-los tots guardats simultàniament.

A l'expressió
`collection.find().forEach((Document doc) -> System.out.println(doc.toJson()));`
cal especificar el tipus de *doc* perquè la declaració de *forEach* especifica
que el paràmetre pot ésser de tipus *Document* o de qualsevol tipus superior.
