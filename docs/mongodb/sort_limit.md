## Ordenació i límits

Una altra operació habitual, tant en SQL com en models orientats a documents,
és limitar el nombre de resultats returnats, així com ordenar-los per algun
dels camps.

Pels següents exemples utilitzarem una col·lecció de punts molt similar a la
de l'apartat anterior:

```java
MongoClient client = new MongoClient();
MongoDatabase db = client.getDatabase("exemples");
MongoCollection<Document> coll = db.getCollection("punts");

coll.drop();

for (int i=0; i<100; i++) {
	int x = ThreadLocalRandom.current().nextInt(10);
	int y = ThreadLocalRandom.current().nextInt(10);
	int z = ThreadLocalRandom.current().nextInt(10);
	Document document = new Document("x",x).append("y", y).append("z", z);
	coll.insertOne(document);
}
```

A la següent consulta volem recuperar les coordenades *x* i *y* de tots els
punts que tenen la coordenada *z* igual a 5. Ordenarem els resultats per la
*x*, de forma ascendent. Els punts que tinguin la mateixa coordenada *x* els
ordenarem entre ells a partir de la coordenada *y*, aquest cop descendent:

```java
Bson filter = eq("z", 5);
Bson projection = fields(include("x","y"), excludeId());
// Ordena primer per x de forma ascendent, i després per y de forma descendent.
Bson sort = new Document("x", 1).append("y", -1);

coll.find(filter).projection(projection).sort(sort).forEach(Document doc -> System.out.println(doc.toJson()));
System.out.println();
```

Per ordenar hem creat un document que associa cada criteri d'ordenació a un 1
si és ascendent i a un -1 si és descendent. Després, hem passat aquest
document al mètode *sort()*.

La consulta equivalent a la terminal de MongoBD és:

```
db.punts.find({"z":{$eq:5}}, {"x":1,"y":1,"_id":0}).sort({"x":1,"y":-1})
```

Repetim la consulta, però aquest volem un màxim de 5 documents:

```java
// Retornem només 5 resultats
coll.find(filter).projection(projection).sort(sort).limit(5).forEach(Document doc -> System.out.println(doc.toJson()));
System.out.println();
```

Això és equivalent a:

```
db.punts.find({"z":{$eq:5}}, {"x":1,"y":1,"_id":0}).sort({"x":1,"y":-1}).limit(5)
```

També podem saltar-nos alguns documents abans d'obtenir els resultats. Per
exemple, en aquest cas ens saltem els 5 primers resultats i després obtenim
els 3 resultats següents (si encara en queden):

```java
coll.find(filter).projection(projection).sort(sort).skip(3).limit(5).forEach((Block<Document>) doc -> System.out.println(doc.toJson()));
System.out.println();
```

A la terminal ho faríem amb:

```
db.punts.find({"z":{$eq:5}}, {"x":1,"y":1,"_id":0}).sort({"x":1,"y":-1}).skip(3).limit(5)
```

Finalment, com en els casos dels filtres i les projeccions, tenim una classe
que ens permet especificar les ordenacions sense haver d'explicitar
l'estructura d'un document.

Es tracta de la classe *Sorts* i, utilitzant-la, podem reescriure la
clàusula d'ordenació de la següent manera:

```java
sort = Sorts.orderBy(Sorts.ascending("x"), Sorts.descending("y"));
```

O, equivalentment, si utilitzem *import static*:

```java
sort = orderBy(ascending("x"), descending("y"));
```

Si només volem ordenar per un camp, o tots els camps els utilitzem de forma
ascendent o descendent, no cal cridar a *orderBy()*:

```java
sort = ascending("x");
```

```java
sort = ascending("x","y");
```

Pots consultar el
[codi complet](codi/mongodb/src/main/java/mongodb/ExempleOrdenacio.java) de
l'exemple.
