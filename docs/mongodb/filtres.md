## Filtres

Habitualment no voldrem recuperar tots els documents d'una col·lecció, sinó
que ens quedarem només amb els que compleixen alguna condició.

El mètode *find()* té alguns paràmetres opcionals que li podem passar. El
primer és justament el criteri de cerca que s'ha d'utilitzar.

En els següents exemples especifiquem alguns dels mètodes comuns per
especificar el criteri de cerca.

Partim de la mateixa col·lecció de punts que en l'apartat de consultes
simples.

Un primer sistema és crear un document d'exemple. Només es retornaran els
documents que tinguin els mateixos valors que el document d'exemple, en tots
els camps especificats.

Per exemple, en aquest segment de codi recuperem tots els punts que tenen
un 3 a la coordenada *x*:

```java
		Bson filter = new Document("x", 3);
		List<Document> result = collection.find(filter).into(new ArrayList<Document>());
		for (Document doc : result) {
			System.out.println(doc.toJson());
		}
		System.out.println();
```

Hem utilitzar *Bson* com a tipus per a *filter* perquè *Document* hereta de
*Bson* i *Bson* és el tipus de retorn d'alguns mètodes que utilitzem més
endavant.

La consulta que acabem de fer és equivalent a la següent consulta en la
línia de comandes de MongoDB:

```
db.punts.find({x:3})
```

A la següent consulta volem els punts que tinguin la coordenada *x* menor
que 3 i la coordenada *y* major o igual a 3.

Tenim dos formes de fer-ho. A la primera utilitzem els operadors *$lt* i
*$gte* de MongoDB:

```java
		// Sintaxi basada en documents
		filter = new Document("x", new Document("$lt", 3))
				.append("y", new Document("$gte", 3));
		result = collection.find(filter).into(new ArrayList<Document>());
		for (Document doc : result) {
			System.out.println(doc.toJson());
		}
```

La consulta que hem creat és equivalent a la consulta:

```
db.punts.find({x:{$lt:3},y:{$gte:3}})
```

El document que hem creat en Java per fer de model és exactament equivalent al
document que hem utilitzat a la consulta de consola.

Una altra forma de fer la mateixa consulta és utilitzant els mètodes *static*
de la classe *Filters* que ens permeten especificar condicions. Així, tindrem
el mètode *Filters.lt()* equivalent a *$lt* i *Filters.gte()* equivalent a
*$gte*.

```java
		// Sintaxi basada en Filters
		filter = Filters.and(Filters.lt("x", 2), Filters.gte("y", 3));
```

Podem millorar la llegibilitat del codi si importem de forma estàtica els
mètodes de *Filters* necessaris:

```java
import static com.mongodb.client.model.Filters.and;
import static com.mongodb.client.model.Filters.gte;
import static com.mongodb.client.model.Filters.lt;
```

Aquests *import* ens permeten utilitzar els mètodes *and*, *gte* i *lt* de
*Filters* sense haver d'especificar que són de *Filters*.

A l'Eclipse podem crear aquest *import* automàticament si ens posem sobre del
nom del mètode i premem *ctrl+shift+m*.

El codi queda així:

```java
		// Igual que l'anterior amb els mètodes de Filters importats de format estàtica
		filter = and(lt("x", 2), gte("y", 3));

		client.close();
	}

}
```

Pots consultar el
[codi complet](codi/mongodb/src/main/java/mongodb/ExempleFiltres.java) de
l'exemple.
