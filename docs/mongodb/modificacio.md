## Modificació de dades

En aquest apartat veurem com podem modificar des de Java les dades que ja
existeixen en una col·lecció de MongoDB.

Farem els exemples sobre una col·lecció molt similar a les dels apartats
anteriors. En aquesta ocasió tindrem uns quants punts bidimensionals:

```java
MongoClient client = new MongoClient();
MongoDatabase db = client.getDatabase("exemples");
MongoCollection<Document> coll = db.getCollection("punts");

coll.drop();

for (int i=0; i<100; i++) {
	Document document = new Document("x",i).append("y", i);
	coll.insertOne(document);
}
```

Al següent exemple prenem el punt que té com a coordenada *x* un 3. Substituïm
aquest document per un document completament nou que té, a més d'una *x* i
una *y*, un camp completament nou:

```java
System.out.println(coll.find(eq("x", 3)).first().toJson());
// Substitueix un document per document completament nou.
coll.replaceOne(eq("x", 3), new Document("x", 3).append("y", 15).append("nou_camp", 1));
System.out.println(coll.find(eq("x", 3)).first().toJson());
```

Veiem que el mètode *replaceOne()* em permet seleccionar un document a partir
d'algun criteri, i reemplaçar-lo completament per un document nou, que pot
tenir una estructura diferent.

La sortida d'aquest segment de codi és la següent:

```
{ "_id" : { "$oid" : "56af9a61ec24140cc4ee54b9" }, "x" : 3, "y" : 3 }
{ "_id" : { "$oid" : "56af9a61ec24140cc4ee54b9" }, "x" : 3, "y" : 15, "nou_camp" : 1 }
```

L'identificador de l'objecte no ha canviat: tot i haver especificat un
document nou, internament es considera que és el mateix document que abans.

En comptes de *replaceOne()* podem utilitzar *updateOne()*. En aquest cas,
passem un document que només pot contenir operadors d'actualització, com ara
*$set*, *$inc* o *$mul*.

El document s'actualitza seguint les indicacions proporcionades en aquest
document:

```java
// Actualitza només alguns dels camps del document existent.
coll.updateOne(eq("x", 3), new Document("$set", new Document("y", 20)));
System.out.println(coll.find(eq("x", 3)).first().toJson());
coll.updateOne(eq("x", 3), new Document("$inc", new Document("y", 20)));
System.out.println(coll.find(eq("x", 3)).first().toJson());
```

El resultat d'executar aquest segment de codi és:

```
{ "_id" : { "$oid" : "56af9a61ec24140cc4ee54b9" }, "x" : 3, "y" : 20, "nou_camp" : 1 }
{ "_id" : { "$oid" : "56af9a61ec24140cc4ee54b9" }, "x" : 3, "y" : 40, "nou_camp" : 1 }
```

En general, *updateOne()* només actualitza un document que ja estigui a la
col·lecció. Si el document no existeix, passarà de llarg.

Si, contràriament, ens interessa que en cas de no existir el document, es
creï, ho podem fer passant un tercer paràmetre a *updateOne()*. Aquest
paràmetre és de tipus *UpdateOptions* i li hem d'indicar que creï els
documents inexistents posant-li la propietat *upsert* a *true*:

```java
// Si intentem editar un document que no existeix, no fa res
coll.updateOne(eq("x", 1000), new Document("$set", new Document("y", 20)));
// la següent instrucció actualitza el document si existeix o l'insereix si no existeix.
// En cas que l'insereixi, el nou document tindrà el seu _id, x=1000 i y=20
coll.updateOne(eq("x", 1000), new Document("$set", new Document("y", 20)), new UpdateOptions().upsert(true));
System.out.println(coll.find(eq("x", 1000)).first().toJson());
```

El document obtingut és:
`{ "_id" : { "$oid" : "56af9a6114321960a4a429db" }, "x" : 1000, "y" : 20 }`.

Una altra possibilitat és modificar molts documents de cop. Per fer-ho,
utilitzarem el mètode *updateMany()*:

```java
		// També podem editar molts documents d'una sola tirada
		// Aquí augmentem en 5 les y de tots els documents que tinguin x més gran que 90:
		coll.updateMany(gt("x", 90), new Document("$inc", new Document("y", 5)));
		coll.find(gt("x", 90)).forEach((Document doc)->System.out.println(doc.toJson()));
```

El resultat d'executar aquesta part és:

```
{ "_id" : { "$oid" : "56af9a61ec24140cc4ee5511" }, "x" : 91, "y" : 96 }
{ "_id" : { "$oid" : "56af9a61ec24140cc4ee5512" }, "x" : 92, "y" : 97 }
{ "_id" : { "$oid" : "56af9a61ec24140cc4ee5513" }, "x" : 93, "y" : 98 }
{ "_id" : { "$oid" : "56af9a61ec24140cc4ee5514" }, "x" : 94, "y" : 99 }
{ "_id" : { "$oid" : "56af9a61ec24140cc4ee5515" }, "x" : 95, "y" : 100 }
{ "_id" : { "$oid" : "56af9a61ec24140cc4ee5516" }, "x" : 96, "y" : 101 }
{ "_id" : { "$oid" : "56af9a61ec24140cc4ee5517" }, "x" : 97, "y" : 102 }
{ "_id" : { "$oid" : "56af9a61ec24140cc4ee5518" }, "x" : 98, "y" : 103 }
{ "_id" : { "$oid" : "56af9a61ec24140cc4ee5519" }, "x" : 99, "y" : 104 }
{ "_id" : { "$oid" : "56af9a6114321960a4a429db" }, "x" : 1000, "y" : 25 }
```

Podem comprovar com s'ha augmentat 5 a les coordenades *y* de tots aquests
punts.

Pots consultar el
[codi complet](codi/mongodb/src/main/java/mongodb/ExempleReplace.java) de
l'exemple.
