## Sentències DML i DDL. Modificació de dades

Les sentències de modificació de dades, com *INSERT*, *DELETE* o *UPDATE*,
funcionen de forma similar a les sentències de consulta.

En el següent exemple inserim una nova fila a la taula *employees*:

```java
public class CrearInsert {
	public static void main(String[] args) {
		String url = "jdbc:mysql://localhost:3306/employees";
		Properties propietatsConnexio = new Properties();
		propietatsConnexio.put("user", "root");
		propietatsConnexio.put("password", "usbw");

		try (Connection con = DriverManager.getConnection(url,propietatsConnexio);
				Statement st = con.createStatement()) {
			int numFiles= st.executeUpdate("INSERT INTO "
					+ "employees(birth_date, first_name, last_name, gender, hire_date) "
					+ "VALUES('1953-03-16','Richard','Stallman','M','1975-01-21')");						
			System.out.println("Inserció creada! Files afectades: " + numFiles);
		} catch (SQLException e) {
			System.err.println("Error SQL: " + e.getMessage());
		}
	}
}
```

A diferències dels exemples anteriors, en aquest codi utilitzem
*executeUpdate()* en comptes d'*executeQuery()*. El mètode *executeUpdate()*
permet enviar quasevol sentències de modificació de dades, així com de
definició de dades (*CREATE*, *DROP*, *ALTER*); retorna un *int*
que és la quantitat de files afectades pel canvi. En l'exemple anterior
retornaria un 1, ja que només hem inserit una fila.

També podem utilitzar sentències preparades per a les modificacions de dades.
En el següent exemple es permet a l'usuari afegir un nou idioma a la taula
*language* de la base de dades *sakila*.

```java
public class InsercioIdiomes {
	public static void main(String[] args) {
		if (args.length == 1) {
			try (Connection connection = DriverManager.getConnection("jdbc:mariadb://localhost/sakila", "root",
					"usbw");
					PreparedStatement comptaIdioma = connection
							.prepareStatement("SELECT count(*) FROM language WHERE name=?");
					PreparedStatement insereixIdioma = connection
							.prepareStatement("INSERT INTO language(name) VALUES (?)");) {
				comptaIdioma.setString(1, args[0]);
				try (ResultSet rs = comptaIdioma.executeQuery()) {
					if (rs.next() && rs.getInt(1) == 0) {
						insereixIdioma.setString(1, args[0]);
						int files = insereixIdioma.executeUpdate();
						if (files == 1) {
							System.out.println("S'ha insertat la fila correctament");
						} else {
							System.err.println("No s'ha pogut insertar la fila");
						}
					} else {
						System.err.println("Aquest idioma ja és a la llista");
					}
				}
			} catch (SQLException e) {
				// Missatge retornat pel SGBD
				System.err.println("Missatge: " + e.getMessage());
				// Codi d'error estàndard
				System.err.println("Estat SQL: " + e.getSQLState());
				// Codi d'error propi del fabricant del SGBD
				System.err.println("Codi de l'error: " + e.getErrorCode());
			}
		} else {
			System.err.println("S'esperava el nom d'un idioma");
		}
	}

}
```

Al final d'aquest exemple, dins del bloc *catch*, podem veure com es pot
utilitzar una *SQLException* per obtenir molta informació sobre el problema
que ha provocat l'excepció.
