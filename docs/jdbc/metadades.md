## Metadades

A més de recuperar les dades emmagatzemades a una base de dades, podem
obtenir informació sobre la base de dades en sí: les seves taules, els tipus
de les seves columnes, quines són les claus forànies, etc. Això és el que es
coneix com a **metadades**.

En el següent exemple recuperem un munt d'informació sobre la base de dades
Sakila.

Comencem per analitzar el mètode *main*:

```java
public static void main(String[] args) {
	DatabaseMetaData dbmd = null;

	try (Connection connection = DriverManager.getConnection
			("jdbc:mariadb://localhost/sakila","root","usbw");) {
		dbmd = connection.getMetaData();
		mostraInfoBD(dbmd);
		mostraTaules(dbmd);
		mostraColumnes(dbmd, "film");
	} catch (SQLException e) {
		System.err.println(e.getMessage());
	}
}
```

*DatabaseMetaData* és la classe que ens dóna accés a realitzar consultes sobre
les metadades de la base de dades on hem connectat.

Primer obtindrem informació general sobre la base de dades, després sobre les
seves taules, i finalment mostrarem informació més detallada sobre la taula
*film*.

A part d'aquests exemples, podríem obtenir molta més informació que ens
interessés.

Al següent mètode recuperem informació general com el nom de la base de dades,
la URL de connexió o amb quin usuari ens hi hem connectat:

```java
public static void mostraInfoBD(DatabaseMetaData dbmd) throws SQLException {
	String nom = dbmd.getDatabaseProductName();
	String driver = dbmd.getDriverName();
	String url = dbmd.getURL();
	String usuari = dbmd.getUserName();

	System.out.println("Informació de la BD:");
	System.out.println("Nom: "+nom);
	System.out.println("Driver: "+driver);
	System.out.println("URL: "+url);
	System.out.println("Usuari: "+usuari);
}
```

La sortida d'aquest mètode és:

```
Informació de la BD:
Nom: MySQL
Driver: MariaDB connector/J
URL: jdbc:mysql://localhost:3306/sakila
Usuari: root
```

A partir de l'objecte *DatabaseMetaData* que hem recuperat abans, podem
realitzar diverses consultes. Aquestes consultes funcionen de forma similar
a les consultes SQL, en el sentit en què ens retornen un *ResultSet* que
podem recórrer per recuperar tots els resultats.

Al mètode següent recuperem totes les taules de la base de dades:

```java
public static void mostraTaules(DatabaseMetaData dbmd) throws SQLException {
	try (ResultSet rs = dbmd.getTables(null, "sakila", null, null);) {
		// Obtenir només les taules:
		// String[] types = {"TABLE"};
		// rs = dbmd.getTables(null, null, null, types);
		while (rs.next()) {
			String cataleg = rs.getString(1);
			String esquema = rs.getString(2);
			String taula = rs.getString(3);
			String tipus = rs.getString(4);
			System.out.println(tipus + " - Cataleg: " + cataleg +
					", Esquema: "+esquema+", Nom: "+taula);
		}
	}
}
```

*getTables()* rep quatre paràmetres que es poden utilitzar per indicar
sobre quines taules volem recuperar informació. *null* indica que no volem
restringir els resultats pel algun dels criteris.

El primer paràmetre és el catàleg. El significat de catàleg pot variar d'un
fabricant a l'altre. En el cas de MySQL el catàleg coincideix amb el nom
de la base de dades.

El segon paràmetre és l'esquema. El MySQL, al contrari que la major part de
SGBD, no fa ús dels esquemes i considera que esquema és sinònim amb el nom
de la base de dades.

El tercer paràmetre es refereix al nom de les taules a recuperar.

Finalment, el quart paràmetre és un array amb els tipus de taules que volem
recuperar. A l'exemples les estem recuperant totes, però si només voleguéssim
les taules pures i no les vistes, podríem indicar en aquest paràmetre un
array amb el contingut `{"TABLE"}`.

La major part d'aquests paràmetres permet l'ús de comodins SQL (*%*).

El *ResultSet* que retorna *getTables()* té un total de 10 columnes, el
significat de les quals es pot consultar a l'ajuda de Java. En aquest
exemple ens hem quedat només amb les més esencials.

El resultat d'executar aquest mètode és el següent:

```
BASE TABLE - Cataleg: sakila, Esquema: null, Nom: actor
BASE TABLE - Cataleg: sakila, Esquema: null, Nom: address
BASE TABLE - Cataleg: sakila, Esquema: null, Nom: category
BASE TABLE - Cataleg: sakila, Esquema: null, Nom: city
BASE TABLE - Cataleg: sakila, Esquema: null, Nom: country
BASE TABLE - Cataleg: sakila, Esquema: null, Nom: customer
BASE TABLE - Cataleg: sakila, Esquema: null, Nom: film
BASE TABLE - Cataleg: sakila, Esquema: null, Nom: film_actor
BASE TABLE - Cataleg: sakila, Esquema: null, Nom: film_category
BASE TABLE - Cataleg: sakila, Esquema: null, Nom: film_text
BASE TABLE - Cataleg: sakila, Esquema: null, Nom: inventory
BASE TABLE - Cataleg: sakila, Esquema: null, Nom: language
BASE TABLE - Cataleg: sakila, Esquema: null, Nom: payment
BASE TABLE - Cataleg: sakila, Esquema: null, Nom: rental
BASE TABLE - Cataleg: sakila, Esquema: null, Nom: staff
BASE TABLE - Cataleg: sakila, Esquema: null, Nom: store
VIEW - Cataleg: sakila, Esquema: null, Nom: actor_info
VIEW - Cataleg: sakila, Esquema: null, Nom: customer_list
VIEW - Cataleg: sakila, Esquema: null, Nom: film_list
VIEW - Cataleg: sakila, Esquema: null, Nom: nicer_but_slower_film_list
VIEW - Cataleg: sakila, Esquema: null, Nom: sales_by_film_category
VIEW - Cataleg: sakila, Esquema: null, Nom: sales_by_store
VIEW - Cataleg: sakila, Esquema: null, Nom: staff_list
```

El mètode *mostraColumnes* ens permet obtenir informació més detallada sobre
una taula en concret:

```java
public static void mostraColumnes(DatabaseMetaData dbmd, String taula) throws SQLException {
	System.out.println("Columnes de la taula: "+taula);
	try (ResultSet columnes = dbmd.getColumns(null, "sakila", taula, null)) {
		while (columnes.next()) {
			String nom = columnes.getString("COLUMN_NAME"); // 4
			String tipus = columnes.getString("TYPE_NAME"); // 6
			String mida = columnes.getString("COLUMN_SIZE"); // 7
			String nula = columnes.getString("IS_NULLABLE"); // 18
			System.out.println("Columna: "+nom+", Tipus: "+tipus+
					", Mida: "+mida+", Pot ser nul·la? "+nula);
		}
	}
}
```

Els paràmetres de *getColumns()* són similars als de *getTables()*, però
l'últim paràmetre fa referència a quines columnes volem recuperar. En el
nostre cas les hem recuperat totes.

El *ResultSet* que retorna *getColumns()* té un total de 24 columnes. Se
n'han seleccionat algunes de significatives per a l'exemple. El resultat és
el següent:

```
Columnes de la taula: film
Columna: film_id, Tipus: SMALLINT UNSIGNED, Mida: 5, Pot ser nul·la? NO
Columna: title, Tipus: VARCHAR, Mida: 255, Pot ser nul·la? NO
Columna: description, Tipus: TEXT, Mida: 65535, Pot ser nul·la? YES
Columna: release_year, Tipus: YEAR, Mida: null, Pot ser nul·la? YES
Columna: language_id, Tipus: TINYINT UNSIGNED, Mida: 3, Pot ser nul·la? NO
Columna: original_language_id, Tipus: TINYINT UNSIGNED, Mida: 3, Pot ser nul·la? YES
Columna: rental_duration, Tipus: TINYINT UNSIGNED, Mida: 3, Pot ser nul·la? NO
Columna: rental_rate, Tipus: DECIMAL, Mida: 4, Pot ser nul·la? NO
Columna: length, Tipus: SMALLINT UNSIGNED, Mida: 5, Pot ser nul·la? YES
Columna: replacement_cost, Tipus: DECIMAL, Mida: 5, Pot ser nul·la? NO
Columna: rating, Tipus: ENUM, Mida: 5, Pot ser nul·la? YES
Columna: special_features, Tipus: SET, Mida: 54, Pot ser nul·la? YES
Columna: last_update, Tipus: TIMESTAMP, Mida: 19, Pot ser nul·la? NO
```

### Metadades de consultes

A banda de recuperar informació sobre la base de dades en sí, amb JDBC
també podem recuperar metadades sobre els resultats de les consultes que
executem.

En el següent exemple realitzem una consulta senzilla sobre la taula *film*
i després examinem el format de les columnes que ha retornat la consulta.
Això ho fem a través de la classe *ResultSetMetaData*:

```java
public class InfoResultSet {
	public static void main(String[] args) {
		ResultSetMetaData rsmd = null;

		try (Connection connection = DriverManager.getConnection
				("jdbc:mariadb://localhost/sakila","root","usbw");
				Statement st = connection.createStatement();
				ResultSet rs = st.executeQuery("SELECT * FROM film LIMIT 3");) {

			rsmd = rs.getMetaData();
			int nCols = rsmd.getColumnCount();
			System.out.println("Columnes recuperades: "+nCols);
			for (int i=1; i<=nCols; i++) {
				System.out.println("Columna: "+i+":");
				System.out.println("  Nom: "+rsmd.getColumnName(i));
				System.out.println("  Tipus: "+rsmd.getColumnTypeName(i));
				System.out.println("  Pot ser nul·la? "+(rsmd.isNullable(i)==0?"No":"Sí"));
				System.out.println("  Amplada màxima de columna: "+rsmd.getColumnDisplaySize(i));
			}
		} catch (SQLException e) {
			System.err.println(e.getMessage());
		}
	}
}
```

El resultat d'executar aquest programa és el següent:

```
Columnes recuperades: 13
Columna: 1:
  Nom: film_id
  Tipus: SMALLINT UNSIGNED
  Pot ser nul·la? No
  Amplada màxima de columna: 5
Columna: 2:
  Nom: title
  Tipus: VARCHAR
  Pot ser nul·la? No
  Amplada màxima de columna: 255
Columna: 3:
  Nom: description
  Tipus: VARCHAR
  Pot ser nul·la? Sí
  Amplada màxima de columna: 65535
Columna: 4:
  Nom: release_year
  Tipus: YEAR
  Pot ser nul·la? Sí
  Amplada màxima de columna: 4
Columna: 5:
  Nom: language_id
  Tipus: TINYINT
  Pot ser nul·la? No
  Amplada màxima de columna: 3
Columna: 6:
  Nom: original_language_id
  Tipus: TINYINT
  Pot ser nul·la? Sí
  Amplada màxima de columna: 3
Columna: 7:
  Nom: rental_duration
  Tipus: TINYINT
  Pot ser nul·la? No
  Amplada màxima de columna: 3
Columna: 8:
  Nom: rental_rate
  Tipus: DECIMAL
  Pot ser nul·la? No
  Amplada màxima de columna: 6
Columna: 9:
  Nom: length
  Tipus: SMALLINT UNSIGNED
  Pot ser nul·la? Sí
  Amplada màxima de columna: 5
Columna: 10:
  Nom: replacement_cost
  Tipus: DECIMAL
  Pot ser nul·la? No
  Amplada màxima de columna: 7
Columna: 11:
  Nom: rating
  Tipus: CHAR
  Pot ser nul·la? Sí
  Amplada màxima de columna: 5
Columna: 12:
  Nom: special_features
  Tipus: CHAR
  Pot ser nul·la? Sí
  Amplada màxima de columna: 54
Columna: 13:
  Nom: last_update
  Tipus: TIMESTAMP
  Pot ser nul·la? No
  Amplada màxima de columna: 19
```
