SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de dades: `onomastica`
--
CREATE DATABASE IF NOT EXISTS `onomastica` DEFAULT CHARSET utf8 COLLATE utf8_spanish_ci;
USE `onomastica`;

-- --------------------------------------------------------

--
-- Estructura de la taula `noms2012`
--

CREATE TABLE IF NOT EXISTS `noms2012` (
  `posicio` int(11) NOT NULL,
  `nom` varchar(20) NOT NULL,
  `sexe` varchar(1) NOT NULL,
  `frequencia` int(11) NOT NULL,
  `percentatge` float NOT NULL,
  PRIMARY KEY (`posicio`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
