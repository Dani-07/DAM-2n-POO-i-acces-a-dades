## Lectura i escriptura de fitxer de dades simples

Sovint volem escriure a un fitxer una colla de dades en format binari.

El format binari té l'avantatge de ser molt compacte: si un *int* ocupa 4 bytes
a memòria, ocuparà 4 bytes al fitxer. En canvi, aquest mateix *int* escrit en
text podria ocupar força més.

A canvi, un fitxer binari no es pot llegir fàcilment amb un editor de textos
qualsevol i el programa que l'ha creat ha de recordar quines dades i en quin
ordre les ha guardades per tal de poder-les recuperar correctament.

Per treballar amb dades el Java ens proporciona les classes
*DataInputStream* i *DataOutputStream*. Aquestes classes permeten llegir i
escriure qualsevol tipus primitiu directament.

### Escriptura d'un fitxer de dades

En el següent exemple guardem a un fitxer una colla de dades en format binari:

```java
public class EscriptorDadesBinaries {

	public static void main(String[] args) {
		byte edat = 15;
		short segonsMinut = 60;
		int segonsHora = 3600;
		float pi = 3.14159F;
		double sqrtDos = 1.414;
		char metres = 'm';
		String acceleracio = "m/s^2";

		try (DataOutputStream escriptor = new DataOutputStream(new FileOutputStream("dades.bin"))) {
			escriptor.writeByte(edat);
			escriptor.writeShort(segonsMinut);
			escriptor.writeInt(segonsHora);
			escriptor.writeFloat(pi);
			escriptor.writeDouble(sqrtDos);
			escriptor.writeChar(metres);
			escriptor.writeChars(acceleracio);
		} catch (FileNotFoundException ex) {
			System.err.println(ex);
		} catch (IOException ex) {
			System.err.println(ex);
		}
	}

}
```

Noteu com s'ha creat l'objecte *DataOutputStream*: primer hem creat un
*FileOutputStream*, que ja permet escriure bytes a un fitxer, i aquest
objecte li hem passat al constructor de *DataOutputStream*. *DataOutputStream*
dividirà les dades que vol escriure en bytes individuals i utilitzarà
el *FileOutputStream* que li hem passat per realitzar l'escriptura d'aquests
bytes.

Aquest esquema és molt habitual en tot el tractament que fa el Java dels
fitxers i es coneix amb el nom de *Decorator pattern*.

Si ara obrim el fitxer que s'ha creat amb un editor hexadecimal veurem el
següent contingut:

```
0f 00 3c 00 00 0e 10 40 49 0f d0 3f f6 9f be 76
c8 b4 39 00 6d 00 6d 00 2f 00 73 00 5e 00 32
```

Podem reconèixer alguna de les dades. Per exemple, el primer 0f és el valor
15 en hexadecimal, que és just el valor que hem guardat per l'*edat*. Els
següents dos bytes són 00 3c que si els passem a decimal dóna 60, cosa que es
correspon amb els dos bytes que ocupa el *short* *segonsMinut*. També
podem observar com els caràcters d'*acceleracio*, al final del fitxer, ocupen
2 bytes cadascun, perquè un *char* en Java ocupa 2 bytes.

### Lectura d'un fitxer de dades

En el següent exemple recuperem les dades que hem guardat a l'exemple anterior.

Noteu que és responsabilitat del programa saber quin tipus de dades, en quin
ordre, i quin significat tenen.

```java
public class LectorDadesBinaries {
	public static void main(String[] args) {
		byte edat;
		short segonsMinut;
		int segonsHora;
		float pi;
		double sqrtDos;
		char metres;
		char[] acceleracio = new char[5];

		try (DataInputStream lector = new DataInputStream(new FileInputStream("dades.bin"))) {
			edat = lector.readByte();
			segonsMinut = lector.readShort();
			segonsHora = lector.readInt();
			pi = lector.readFloat();
			sqrtDos = lector.readDouble();
			metres = lector.readChar();
			for (int i=0;i<5;i++)
				acceleracio[i]=lector.readChar();

			System.out.format("%d %d %d %f %f %c %s", edat, segonsMinut, segonsHora,
					pi, sqrtDos, metres, new String(acceleracio));
		} catch (FileNotFoundException ex) {
			System.err.println(ex);
		} catch (IOException ex) {
			System.err.println(ex);
		}
	}
}
```

*DataOutputStream* té el mètode *writeChars* que permet escriure un array
de caràcters en una sola crida. En canvi, *DataInputStream* no té un
mètode equivalent per llegir tots els caràcters de cop. Això és així
perquè no tindria forma de saber quants caràcters ha de llegir.
