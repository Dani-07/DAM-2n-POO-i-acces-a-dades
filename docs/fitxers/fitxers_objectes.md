## Lectura i escriptura d'objectes a fitxers binaris

A l'apartat anterior hem vist com podem crear fitxers binaris guardant-hi
dades simples. Sovint però voldrem guardar objectes sencers a fitxers, de
manera que sigui senzill recuperar després aquests objectes en un futur.

Java ens facilita la feina de guardar i recuperar objectes amb les classes
*ObjectOutputStream* i *ObjectInputStream*.

Per tal que els objectes d'una classe es puguin guardar i carregar
utilitzant aquest sistema cal que implementin la interfície **Serializable**.
Aquesta interfície no té cap mètode, serveix només per marcar que els
objectes d'aquella classe es poden guardar en fitxers.

En aquest apartat treballarem amb aquesta senzilla, que l'única novetat que
presenta és que implementa *Serializable*:

```java
public class Mascota implements Serializable {
	private static final long serialVersionUID = 1L;

	private String nom;
	private int numPotes;
	private boolean pel = true;

	public Mascota(String nom, int numPotes) {
		this.nom=nom;
		this.numPotes=numPotes;
	}

	public Mascota(String nom, int numPotes, boolean pel) {
		this(nom,numPotes);
		this.pel=pel;
	}

	public String getNom() {
		return nom;
	}

	public int getNumPotes() {
		return numPotes;
	}

	public boolean hasPel() {
		return pel;
	}

	@Override
	public String toString() {
		return "Mascota nom="+nom+" numPotes="+numPotes+" pel="+pel;
	}

}
```

Un problema que podem tenir amb la recuperació d'un objecte que s'ha guardat
amb aquest sistema és si la classe s'ha modificat enmig. Imaginem que guardem
una mascota com la que hem definit en un fitxer, més tard modifiquem la classe
i afegim l'atribut *especie* i traiem l'atribut *pel*. Quan intentem tornar
a llegir aquest fitxer està clar que no podrem.

Per facilitar al Java la detecció d'aquests conflictes es posa a totes les
classes *Serializable* un atribut estàtic anomenat *serialVersionUID* amb
un número de sèrie. Aquest número s'ha de canviar cada cop que es facin
canvis a la classe que facin incompatible la càrrega d'objectes creats
amb versions anteriors.

### Escriptura d'objectes

En el següent exemple creem una colla d'objectes *Mascota* i els guardem
en un fitxer:

```java
public class EscriptorObjectes {

	public static void main(String[] args) {
		Mascota[] mascotes = new Mascota[4];
		mascotes[0] = new Mascota("Rudy", 4);
		mascotes[1] = new Mascota("Piolin", 2, false);
		mascotes[2] = new Mascota("Nemo", 0, false);
		mascotes[3] = new Mascota("Tara", 8);

		try (ObjectOutputStream escriptor = new ObjectOutputStream(new FileOutputStream("prova.bin"));) {
			for (Mascota m : mascotes) {
				escriptor.writeObject(m);
			}
		} catch (IOException ex) {
			System.err.println(ex);
		}
	}
}
```

### Lectura d'objectes

A continuació recuperem les mascotes que hem guardat a l'exemple anterior.

Aquest programa suposa que no coneixem quantes mascotes hem guardat i
les va recuperant fins que es produeix una excepció de tipus
*EOFException*, indicant que hem intentat llegir més enllà del final
del fitxer.

```java
public class LectorObjectes {
	public static void main(String[] args) {
		List<Mascota> mascotes = new ArrayList<Mascota>();

		try (ObjectInputStream lector = new ObjectInputStream(new FileInputStream("prova.bin"));) {
			while (true) {
				Object o = lector.readObject();
				if (o instanceof Mascota) {
					mascotes.add((Mascota)o);
				}
			}

		} catch (EOFException ex) {
			System.out.println("Hem arribat a final de fitxer.");
			for (Mascota m : mascotes) {
				System.out.println(m);
			}
		} catch (IOException ex) {
			System.err.println(ex);
		} catch (ClassNotFoundException ex) {
			System.err.println(ex);
		}
	}
}
```

Quan recuperem un objecte amb *readObject* ens retorna una referència de
tipus *Object*, encara que l'objecte sigui d'un altre tipus. Cal fer un
*cast* per transformar la referència a la classe a la qual realment
pertany l'objecte.

Amb aquest sistema és possible guardar objectes de diferents classes al
mateix fitxer. A l'hora de recuperar-los és útil utilitzar l'operador
*instanceof* per tractar cada tipus d'objecte de la forma adequada.

### Lectura i escriptura d'estructures complexes d'objectes

Un avantatge que té aquest sistema de desar objectes a fitxers és que el
Java s'encarrega de guardar automàticament les referències a altres
objectes i s'encarrega de guardar només un cop cada objecte si apareix
més d'una vegada.

Naturalment, només es guardaran les referències a altres objectes de
classes que també siguin *Serializable*. Les referències a altres objectes
les haurà de restaurar l'aplicació en el moment de carregar els objectes.

Exemples d'objectes que no es guarden a fitxer poden ser les connexions a
xarxa o a bases de dades, així com qualsevol atribut que marquem amb el
modificador *transient*.

Per tal de resoldre tots aquests detalls, es poden crear dos mètodes
especials anomenats *readObject* i *writeObject* que es cridaran
respectivament en el procés de llegir i desar un objecte.

Aquí no entrarem en tant de detall i només comprovarem com les
referències a altres objectes es guarden automàticament.

Imaginem la següent classe *Persona*. Una *Persona* pot tenir diverses
mascotes:

```java
public class Persona implements Serializable {
	private static final long serialVersionUID = 1L;

	private String nom;
	private List<Mascota> mascotes = new ArrayList<Mascota>();

	public Persona(String nom) {
		this.nom=nom;
	}

	public String getNom() {
		return nom;
	}

	public void afegeixMascota(Mascota mascota) {
		mascotes.add(mascota);
	}

	public List<Mascota> getMascotes() {
		return Collections.unmodifiableList(mascotes);
	}

	@Override
	public String toString() {
		return "Persona "+nom;
	}
}
```

El següent programa crea una persona que tindrà dues mascotes, guarda
la persona a fitxer, i quan la recupera, podem observar com també s'han
recuperat les seves mascotes.

```java
public class EstructuresComplexes {

	public static void main(String[] args) {
		Persona maria = new Persona("Maria");
		maria.afegeixMascota(new Mascota("Rudy", 4));
		maria.afegeixMascota(new Mascota("Piolin", 2, false));

		try (ObjectOutputStream escriptor = new ObjectOutputStream(new FileOutputStream("prova.bin"));) {
			escriptor.writeObject(maria);
		} catch (IOException ex) {
			System.err.println(ex);
		}

		Persona p = null;
		try (ObjectInputStream lector = new ObjectInputStream(new FileInputStream("prova.bin"));) {
			p = (Persona) lector.readObject();
		} catch (IOException ex) {
			System.err.println(ex);
		} catch (ClassNotFoundException ex) {
			System.err.println(ex);
		}

		if (p != null) {
			System.out.println(p);
			for (Mascota mascota : p.getMascotes()) {
				System.out.println(mascota);
			}
		}
	}

}
```

La sortida del programa és:

```
Persona Maria
Mascota nom=Rudy numPotes=4 pel=true
Mascota nom=Piolin numPotes=2 pel=false
```
