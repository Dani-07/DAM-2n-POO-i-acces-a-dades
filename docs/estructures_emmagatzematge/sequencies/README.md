### Seqüències

Les **seqüències** són estructures dinàmiques que contenen un nombre
variable d'elements ordenats en una successió. Dins d'aquest grup
d'estructures estudiarem les *piles*, les *cues* i les *llistes*.

 * [Interfícies](interficies.md)
 * [Implementacions](implementacions.md)
 * [Genèrics](generics/README.md)
 * [Iteradors](iteradors/README.md)
 * [Algorismes](algorismes.md)
