import java.util.ArrayList;

import java.util.ArrayList;
import java.util.List;

public class ProdueixEsdeveniments {
	List<Listener> listasListener = new ArrayList<>();
	
	public void addEventListener(Listener l){
		listasListener.add(l);
	}
	public void creaEsdeveniment(int nombre) {
		for (Listener listener : listasListener) {
			listener.notifyEvent(nombre);
		}
		
	}
}
