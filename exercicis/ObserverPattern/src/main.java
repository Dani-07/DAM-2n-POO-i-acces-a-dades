
public class main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		GuardaEsdeveniments guarda = new GuardaEsdeveniments();
		MostraEsdeveniments mostra = new MostraEsdeveniments ();
		ProdueixEsdeveniments produeix = new ProdueixEsdeveniments();
		
		produeix.addEventListener(guarda);
		produeix.addEventListener(mostra);
		
		
		produeix.creaEsdeveniment(8);
		produeix.creaEsdeveniment(9);
		produeix.creaEsdeveniment(3);
		
		System.out.println(guarda.toString());
	}

}
