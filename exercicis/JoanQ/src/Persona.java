import java.util.Comparator;
import java.util.Random;

public class Persona implements Comparable<Persona>{
	private int pes;
	private int alcada;
	private int edat;

	private Random random = new Random();

	public Persona() {
		this.pes =  random.nextInt(200);
		this.alcada = random.nextInt(200);
		this.edat = random.nextInt(100);
	}

	public int getEdat() {
		return edat;
	}
	public int getPes() {
		return pes;
	}

	public int getAlcada() {
		return alcada;
	}

	@Override
	public int compareTo(Persona p) {
		return getAlcada() - p.getAlcada();
	}

	public static final Comparator<Persona> COMPARADOR_EDAT = new Comparator<Persona>(){
		@Override
		public int compare(Persona v1, Persona v2) {
			return v1.getEdat()-(v2.getEdat());
		}
	};

	public static final Comparator<Persona> COMPARADOR_PES = new Comparator<Persona>(){
		@Override
		public int compare(Persona v1, Persona v2) {
			return v1.getPes()-(v2.getPes());
		}
	};
}
