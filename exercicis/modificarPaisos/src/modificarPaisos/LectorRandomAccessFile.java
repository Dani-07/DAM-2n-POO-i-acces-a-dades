package modificarPaisos;

import java.io.EOFException;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Scanner;

public class LectorRandomAccessFile {

	
    private Scanner scanIn = new Scanner(System.in);
	
	
    public static String readChars(RandomAccessFile fitxer, int nChars) throws IOException {
        StringBuilder b = new StringBuilder();
        char ch = ' ';
        for (int i=0; i<nChars; i++) {
            ch=fitxer.readChar();
            if (ch != '\0')
                b.append(ch);
        }
        return b.toString();
    }

    public void buscaPais (int index){
        String nom = null, capital = null, codiISO = null;
        int poblacio = 0;
        int cont=0;
        int pos=0;
        int nuevoInt;
        String op = new String();
    	String nuevo = new String();
    	StringBuilder b = null;
       
        try (RandomAccessFile fitxer = new RandomAccessFile("paisos.dat", "rw")) {
            while (fitxer.getFilePointer() != fitxer.length()) {
                if(cont==index){
	            	System.out.println("Pa�s "+nom+ " codigo ISO: " +codiISO +" capital: "+ capital+ ", " + "poblaci�: "+poblacio);
	               	
	            	System.out.println("Introduce lo que quieres modificar");
	            	op = scanIn.nextLine();
	            	System.out.println("Introduce el nuevo " +op);
	            	nuevo = scanIn.nextLine();
	            	if (op.equals("pais")){
	            		fitxer.seek(pos- 170);
	            		b = new StringBuilder(nuevo);
	            	   	b.setLength(40);
	            	   	fitxer.writeChars(b.toString());
	            	   	fitxer.seek(pos- 170);
	            	    nom = readChars(fitxer, 40);
	            	    }
	            	    	
	            	if (op.equals("iso")){
	            	    fitxer.seek(pos- 90);
		            	b = new StringBuilder(nuevo);
		            	b.setLength(40);
		            	fitxer.writeChars(b.toString());
		            	fitxer.seek(pos- 90);
		            	codiISO = readChars(fitxer, 3);
	            	 }
	            	    
	            	if (op.equals("capital")){
		            	fitxer.seek(pos- 80);
		            	b = new StringBuilder(nuevo);
			            b.setLength(40);
			            fitxer.writeChars(b.toString());
			            fitxer.seek(pos- 80);
			            capital = readChars(fitxer, 40);
	            	 }
	            	    	
	            	if (op.equals("poblacio")){
	            		fitxer.seek(pos- 4);
			            nuevoInt= Integer.parseInt(nuevo);
			            fitxer.writeInt(nuevoInt);
			            fitxer.seek(pos- 4);
			            poblacio = fitxer.readInt();
	            	 }	 
	            	    
	            	    System.out.println("Pa�s "+nom+ " codigo ISO: " +codiISO +" capital: "+ capital+ ", " + "poblaci�: "+poblacio);
	            	    
                }
                cont= fitxer.readInt();
	            nom = readChars(fitxer, 40);
	            codiISO = readChars(fitxer, 3);
	            capital = readChars(fitxer, 40);
	            poblacio = fitxer.readInt();
	            pos= (int) fitxer.getFilePointer();
               
            }
        }catch (IOException e) {
            System.err.println(e);
        }
        
    }
    
    

    
}

