import java.util.Arrays;
import java.util.Random;

public class Tirada implements Comparable<Tirada>, Cloneable{
	private int atac;
	private int defensa;
	private int [] tiradas;
	private int numDaus;
	

	private static final Random random = new Random();

	public Tirada(int atac, int defensa, int numDaus) {
		this.atac= atac;
		this.defensa=defensa;
		this.numDaus= numDaus;
		this.tiradas= new int [numDaus];
		for(int i = 0; i < numDaus; i++){
			tiradas[i] = random.nextInt(6)+1;
		}
		Arrays.sort(tiradas);
	}

	public String toString(){
		StringBuffer tiradatotal = new StringBuffer();
		for (int i=0; i<tiradas.length; i++){
			tiradatotal= tiradatotal.append(tiradas[i]+ " ");
		}
		
		return "El ataque del jugador es:  " +this.atac + " ,La defensa del jugador es: " + this.defensa + " i la tirada: " + tiradatotal;

	};
	
	public int compareTo(Tirada t){
		int j1 = 0;
		int j2 = 0;
		int difDaus= this.numDaus-t.numDaus;
		
		for (int i=0; i<=difDaus; i++){
			if ((this.tiradas[i]+this.atac)> (t.tiradas[i]+t.defensa) ){
				j1++;
			}
			if ((t.tiradas[i]+this.atac)> (this.tiradas[i]+this.defensa)){
				j2++;
			}
		}
		for (int i=difDaus; i<this.tiradas.length-1; i++){
			if ((this.tiradas[i]+this.atac)> (0+t.defensa) ){
				j1++;
			}
		}
		
		/*for (i=0; i<difDaus; i++){
				if ((0)> (t.tiradas[i]+t.defensa) ){
					j1++;
				}
		}
		*/
		return j1-j2;
	}
		
}