package jardi;

import java.io.Serializable;

public class Altibus extends Planta implements Serializable{
	@Override
	public Llavor creix(){
		super.creix();
		if (this.getAltura()>7 && this.getAltura()<10){
			Altibus a = new Altibus();
			Llavor ll = new Llavor(a);
			return ll;	
		}else{
			return null;
		}	
	}
	
	@Override
	public char getChar (int nivell){
		if (nivell>this.getAltura()){
			return ' '; 
		}
		else if (this.getAltura()==nivell){
			return 'O';
		}else{
			return '|';
			}
	}

}

