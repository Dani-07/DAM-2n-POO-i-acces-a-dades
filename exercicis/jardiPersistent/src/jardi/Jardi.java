package jardi;

import java.io.Serializable;

public class Jardi implements Serializable{
	private Planta jardi[];
	private int mida;
	
	public Jardi(int mida) {
		if (mida<=0){
			this.mida=10;
			jardi = new Planta[mida];
		}else{
			this.mida=mida;
			jardi = new Planta[mida];
		}
	}
	public void temps(){
		int novaPlanta=0; 
		Planta miLlavor;
		for (int i=0; i<mida; i++){
			if (this.jardi[i]!=null){
				miLlavor=jardi[i].creix();
						while (novaPlanta==0){
							novaPlanta=jardi[i].escampaLlavor();
						}
						if (miLlavor instanceof Planta){
							if (i+novaPlanta>=0 && i+novaPlanta<this.jardi.length && this.jardi[i+novaPlanta]==null){
								if (miLlavor instanceof Llavor){
									jardi[i+novaPlanta]=miLlavor;
								}else{
									this.jardi[i]=miLlavor;
								}
							}
						}
						if (this.jardi[i].isEsViva()==false){
							this.jardi[i]=null;
						}
				}
		}
	}
	

	public String toString(){
				String dibujo = new String();
				char aux;
				int i;
				int j;
				
			for (i=10; i>=0; i--){
				for (j=0; j<this.jardi.length; j++){
					if(i==0)
						aux='-';
					else if(this.jardi[j] != null){
							aux=this.jardi[j].getChar(i);
					}
					else
						aux=' ';
						
					dibujo = dibujo + aux;
				}
				dibujo = dibujo + "\n";
			}
				
			return dibujo;

	
		
	}

	public boolean plantaLlavor(Planta novaPlanta, int pos){
		if (this.jardi[pos]==null && pos<=this.mida){
			this.jardi[pos]=novaPlanta;
			return true;
		}else{
			return false;
		}
		
			
	}



}


