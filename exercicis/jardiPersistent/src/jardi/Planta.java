package jardi;

import java.io.Serializable;
import java.util.Random;

public abstract class Planta implements Serializable {


	private boolean esViva;
	private int altura;
	
	
	public boolean isEsViva() {
		return esViva;
	}
	
	
	public Planta() {
		super();
		this.altura=1;
		this.esViva = true;
	}



	public void setEsViva(boolean esViva) {
		this.esViva = esViva;
	}

	
	
	public void setAltura(int altura) {
		this.altura = altura;
	}

	private static final Random random = new Random();
	
	public char getChar(int nivell){
		return 0;
	};
	
	public Planta creix(){
		if (this.altura<10){
			this.altura++;
		}
		if (this.altura==10){
			this.esViva=false;
		}
		return null;
	};
	
	public int escampaLlavor(){
		return random.nextInt(5)-3+1;
	};
	
	public int getAltura(){
		return this.altura;
	}
	
	public boolean esViva(){
		return this.esViva;
	};

}

