package jardi;

import java.io.Serializable;

public class Declinus extends Planta implements Serializable{
	private int intentsCreixer;
	private boolean haCrecido;
	private boolean segundaLlavor;

	
	public Declinus(){
		super();
		
	}
	@Override
	public Llavor creix(){
		Llavor semilla = null;
		this.intentsCreixer++;
		if (intentsCreixer%2==0){
			
			if (this.getAltura()==4){
				this.haCrecido=true;
				this.segundaLlavor=true;
				this.setAltura(this.getAltura()-1);
				Planta planta = new Declinus();
				semilla= new Llavor(planta);
			}
			
			if(haCrecido==true){
				if (this.getAltura()<=3){
					this.setAltura(this.getAltura()-1);
				}if (this.getAltura()==0){
					this.setEsViva(false);
				}
				if (this.segundaLlavor=true){
					this.segundaLlavor=false;
					Planta planta = new Declinus();
					semilla= new Llavor(planta);
				}
			}
							
			
			if(this.getAltura()<=4 && haCrecido==false){
				this.setAltura(this.getAltura()+1);
			}
		}
		return semilla;
	}
	@Override
	public char getChar (int nivell){
		if (nivell>this.getAltura()){
			return ' '; 
		}
		else if (this.getAltura()==nivell){
				return '*';
			}else{
				return ':';
			}
	}

}


