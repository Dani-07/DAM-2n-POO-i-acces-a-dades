package jardi;

import java.io.Serializable;

public class Llavor extends Planta implements Serializable {
	Planta planta;
	int temps;
	
	
	public Llavor(Planta p) {
		
		if (p instanceof Planta){
			this.planta = p;
			this.setEsViva(true);
		}else{
			throw new IllegalArgumentException("Una llavor no pot produir una altra llavor");
		}
		
	}
	@Override
	public Planta creix(){
		if (this.temps<5){
			this.temps++;
			return null;
		}else{
			return this.planta;
		}	
	}
	
	public char getChar (int nivell){
		if (this.planta.getAltura()==nivell){
			return '.';
		}else{
			return ' ';
		}
	}
	

}
