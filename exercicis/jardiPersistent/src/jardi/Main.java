package jardi;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		Planta al= new Altibus();
		Planta dl= new Declinus();
		Jardi j= new Jardi(10);
		
		
		j.plantaLlavor(dl, 2);
		j.plantaLlavor(al, 6);
		Scanner scanIn = new Scanner(System.in);
		String op = new String();
		op="";
		
        try (ObjectInputStream lector = new ObjectInputStream(new FileInputStream("jardi.sav"));) {
            j = (Jardi) lector.readObject();
            System.out.println("Carga de partida");
        } catch (IOException ex) {
            System.err.println(ex);
        } catch (ClassNotFoundException ex) {
            System.err.println(ex);
        }
		
		while (!op.equals("surt")){
			j.temps();
			System.out.print(j.toString());
			System.out.println("Continua? ");
			op = scanIn.nextLine();
		}
		
		try (ObjectOutputStream escriptor = new ObjectOutputStream(new FileOutputStream("jardi.sav"));) {
                escriptor.writeObject(j);
                System.out.println("Guardo partida");
            
        } catch (IOException ex) {
            System.err.println(ex);
        }
	
	}

}
