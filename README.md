# Cicles d'informàtica de l'Institut Sabadell

## 2n de DAM - Programació orientada a objectes i accés a dades

### Curs 2015-2016

[Configuració de l'entorn de desenvolupament](docs/entorn_desenvolupament/README.md)

## M3 - Programació

### UF 4: programació orientada a objectes (POO). Fonaments.

### UF 5: POO. Llibreries de classes fonamentals.

 1. [Utilització avançada de classes](docs/utilitzacio_avancada_de_classes/README.md)
 * [Estructures d'emmagatzematge](docs/estructures_emmagatzematge/README.md)
 * [Interfícies gràfiques](docs/interficies_grafiques/README.md)

### UF 6: POO. Introducció a la persistència en BD.

 1. [Accés a bases de dades amb JDBC](docs/jdbc/README.md)

## M5 - Entorns de desenvolupament

### UF 3: introducció al disseny orientat a objectes.

 1. [Diagrames UML](docs/diagrames_uml/README.md)

## M6 - Accés a dades

### UF 1: persistència en fitxers.

 1. [Persistència en fitxers](docs/fitxers/README.md)

### UF 2: persistència en BDR-BDOR-BDOO.

### UF 3: persistència en BD natives XML.

 1. [Persistència en BD orientades a documents](docs/mongodb/README.md)

### UF 4: components d’accés a dades.
