package fungus;

import java.util.Random;

public class Cultiu {
	private static final Random RANDOM = new Random();
	private Fungus[][] terreny;
	
	public Cultiu(int nFiles, int nCols) {
		terreny = new Fungus[nFiles][nCols];
	}
	
	public int getNFiles() {
		return terreny.length;
	}
	
	public int getNCols() {
		return terreny[0].length;
	}
	
	public void setFungus(Fungus fungus, int fila, int col) {
		if (terreny[fila][col] != null) {
			terreny[fila][col].getColonia().disminueixPoblacio();
		}
		fungus.setFila(fila);
		fungus.setCol(col);
		fungus.getColonia().augmentaPoblacio();
		terreny[fila][col] = fungus;
	}
	
	public Fungus getFungus(int fila, int col) {
		return terreny[fila][col];
	}
	
	public void temps() {
		int fila = RANDOM.nextInt(getNFiles());
		int col = RANDOM.nextInt(getNCols());
		
		Fungus fungus = terreny[fila][col];
		if (fungus != null) {
			fungus.creix(this);
		}
	}
	
	public void dibuixa() {
		for (Fungus[] fila : terreny) {
			for (Fungus fungus : fila) {
				if (fungus != null)
					System.out.print(fungus.getChar());
				else
					System.out.print(".");
			}
			System.out.println();
		}
		System.out.println();
	}
	
	public boolean esDins(int fila, int col) {
		return fila>=0 && col>=0 && fila<getNFiles() && col<getNCols();
	}
}
