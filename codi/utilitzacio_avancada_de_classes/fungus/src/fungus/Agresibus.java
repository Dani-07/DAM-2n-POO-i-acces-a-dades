package fungus;

public class Agresibus extends Fungus implements Arrencable {
	private final static int[] DIF_FILA = {-1, 0, 1, 0};
	private final static int[] DIF_COL = {0, 1, 0, -1};

	public Agresibus(Colonia colonia) {
		super(colonia);
	}

	@Override
	public void creix(Cultiu cultiu) {
		int novaFila, novaCol;
		for (int i=0; i<DIF_FILA.length; i++) {
			novaFila = getFila()+DIF_FILA[i];
			novaCol = getCol()+DIF_COL[i];
			new Agresibus(getColonia()).posa(cultiu, novaFila, novaCol);
		}
	}
	
	@Override
	protected boolean posa(Cultiu cultiu, int novaFila, int novaCol) {
		boolean potPosar = false;
		if (cultiu.esDins(novaFila, novaCol)) {
			cultiu.setFungus(this, novaFila, novaCol);
			potPosar = true;
		}
		return potPosar;
	}

	@Override
	public char getChar() {
		return 'x';
	}

}
