package ex3;

import java.util.Arrays;
import java.util.Random;

/**
 * Classe utilitzada per calcular una tirada de daus i
 * poder comparar dues tirades entre elles.
 */
public class Tirada implements Comparable<Tirada>, Cloneable {
	private static Random random = new Random();
	private int[] daus;
	private int at;
	private int def;

	public int getAt() {
		return at;
	}

	public int[] getDaus() {
		return daus;
	}

	public int getDef() {
		return def;
	}

	public Tirada(int at, int def, int nDaus) {
		this.at = at;
		this.def = def;
		daus = new int[nDaus];
		for (int i=0; i<nDaus; i++)
			daus[i] = random.nextInt(6)+1;
		Arrays.sort(daus);
	}

	@Override
	public int compareTo(Tirada t) {
		int exits = 0;
		int texits = 0;
		int atac;
		int defensa;
		int index=0, indexT=0;
		
		if (daus.length > t.daus.length) {
			for (index=0; index<daus.length-t.daus.length; index++) {
				if (daus[index]+at>t.def)
					exits++;
			}
		}
		if (t.daus.length > daus.length) {
			for (indexT=0; indexT<t.daus.length-daus.length; indexT++) {
				if (t.daus[indexT]+t.at>def)
					texits++;
			}
		}
		for (; index<daus.length; index++, indexT++) {
			atac = daus[index]+at;
			defensa = t.daus[indexT]+t.def;
			if (atac > defensa)
				exits++;
			atac = t.daus[indexT]+t.at;
			defensa = daus[index]+def;
			if (atac > defensa)
				texits++;
		}
		return exits - texits;
	}
	
	@Override
	public Tirada clone() {
		Tirada t = null;
		try {
			t = (Tirada) super.clone();
		} catch (CloneNotSupportedException e) {
			// Mai hauria d'arribar aqui
			throw new AssertionError(e);
		}
		t.daus = Arrays.copyOf(daus, daus.length);
		
		return t;
	}
	
	@Override
	public String toString() {
		String s = String.format("AT: %d  DEF: %d  Daus (%d): ", at, def, daus.length);
		for (int i=0; i<daus.length; i++)
			s += daus[i];
		return s;
	}
}
