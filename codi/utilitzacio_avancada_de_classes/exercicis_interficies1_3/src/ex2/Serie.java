package ex2;

public interface Serie {
	/**
	 * Inicialitza la sèrie a un valor per defecte
	 */
	public void inicialitza();
	/**
	 * Inicialitza la sèrie a una llavor donar
	 * 
	 * @param llavor  El nombre que s'utilitza per inicialitzar la sèrie
	 */
	public void inicialitza(double llavor);
	
	/**
	 * Cada cop que es crida, calcula i retorna el següent nombre
	 * de la sèrie numèrica.
	 * 
	 * @return  El següent nombre de la sèrie.
	 */
	public double seguent();
}
