package ex2;

/**
 * Aquesta classe implementa la sèrie en què cada
 * element es calcula com el doble de l'anterior.
 */
public class PerDos implements Serie {
	private double actual;

	public PerDos() {
		inicialitza();
	}
	
	public PerDos(double llavor) {
		inicialitza(llavor);
	}
	
	@Override
	public void inicialitza() {
		actual = 1;
	}

	@Override
	public void inicialitza(double llavor) {
		actual = llavor;
	}

	@Override
	public double seguent() {
		actual *= 2;
		return actual;
	}
}
