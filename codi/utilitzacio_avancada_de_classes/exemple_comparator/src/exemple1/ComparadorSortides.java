package exemple1;

import java.util.Comparator;

/**
 * Aquest comparador compara dos vols en funció de l'hora de sortida.
 */
public class ComparadorSortides implements Comparator<VolReal> {

	@Override
	public int compare(VolReal v1, VolReal v2) {
		return v1.getSortida().compareTo(v2.getSortida());
	}

}
