package mongodb;

import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Filters.gt;

import org.bson.Document;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.UpdateOptions;

public class ExempleReplace {

	public static void main(String[] args) {
		MongoClient client = new MongoClient();
		MongoDatabase db = client.getDatabase("exemples");
		MongoCollection<Document> coll = db.getCollection("punts");
		
		coll.drop();
		
		for (int i=0; i<100; i++) {
			Document document = new Document("x",i).append("y", i);
			coll.insertOne(document);
		}

		System.out.println(coll.find(eq("x", 3)).first().toJson());
		// Substitueix un document per document completament nou.
		coll.replaceOne(eq("x", 3), new Document("x", 3).append("y", 15).append("nou_camp", 1));
		System.out.println(coll.find(eq("x", 3)).first().toJson());
		
		// Actualitza només alguns dels camps del document existent.
		// El document d'actualització només pot tenir operadors d'actualització ($set, $inc, $mul...)
		coll.updateOne(eq("x", 3), new Document("$set", new Document("y", 20)));
		System.out.println(coll.find(eq("x", 3)).first().toJson());
		coll.updateOne(eq("x", 3), new Document("$inc", new Document("y", 20)));
		System.out.println(coll.find(eq("x", 3)).first().toJson());
		
		// Si intentem editar un document que no existeix, no fa res
		coll.updateOne(eq("x", 1000), new Document("$set", new Document("y", 20)));
		// la següent instrucció actualitza el document si existeix o l'insereix si no existeix.
		// En cas que l'insereixi, el nou document tindrà el seu _id, x=1000 i y=20
		coll.updateOne(eq("x", 1000), new Document("$set", new Document("y", 20)), new UpdateOptions().upsert(true));
		System.out.println(coll.find(eq("x", 1000)).first().toJson());
		
		// També podem editar molts documents d'una sola tirada
		// Aquí augmentem en 5 les y de tots els documents que tinguin x més gran que 90:
		coll.updateMany(gt("x", 90), new Document("$inc", new Document("y", 5)));
		coll.find(gt("x", 90)).forEach((Document doc)->System.out.println(doc.toJson()));
		
		client.close();
	}

}
