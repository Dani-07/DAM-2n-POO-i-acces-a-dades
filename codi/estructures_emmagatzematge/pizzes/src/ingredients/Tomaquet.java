package ingredients;

import base.Ingredient;
import base.IngredientExtra;

public class Tomaquet extends IngredientExtra implements Cloneable {
	public Tomaquet(Ingredient base) {
		super(base);
	}
	@Override
	public String recepta() {
		String rec = super.recepta() + "\n";
		return rec + "El cuiner utilitza una espàtula per escampar tomàquet per sobre la base.";
	}
	@Override
	public double getPreu() {
		double preu = super.getPreu();
		return preu + 1.5;
	}
	@Override
	public String descripcio() {
		String desc = super.descripcio()+", ";
		return desc + "tomàquet";
	}
	@Override
	public Tomaquet clone() throws CloneNotSupportedException {
		return (Tomaquet) super.clone();
	}

}
