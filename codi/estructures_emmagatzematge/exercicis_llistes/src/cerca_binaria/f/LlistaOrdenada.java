package cerca_binaria.f;

import java.util.ArrayList;
import java.util.List;

/*
 * Estem dient que la llista ordenada guardarà objectes d'un cert tipus,
 * que anomenem T, que compleix que és un tipus derivat (o que implementa)
 * Comparable.
 * Al mateix temps, com que Comparable és una interfície genèrica, li hem
 * de dir de quin tipus són els objectes que pot comparar. Li diem que
 * ha de poder-se comparar amb altres objectes de tipus T.
 */
public class LlistaOrdenada<T extends Comparable<T>> {
	/*
	 * La llista interna que fem servir guardarà objectes de tipus T, que
	 * és el tipus que hem declarat abans
	 */ 
	private List<T> llista = new ArrayList<T>();
	
	/*
	 * Cercarem a la llista un objecte de tipus T
	 */
	public int binarySearch(T element) {
		int pos = -1;
		int inici = 0;
		int fi = llista.size() - 1;
		int mig;
		int comparacio;
		// i trobarem objectes de tipus T dins de la llista
		T elementMig;
		while (fi - inici >= 0) {
			mig = (inici + fi) / 2;
			elementMig = llista.get(mig);
			comparacio = element.compareTo(elementMig);
			if (comparacio == 0) {
				pos = mig;
				inici = fi + 1;
			} else if (comparacio < 0) {
				fi = mig - 1;
			} else { // element > elementMig
				inici = mig + 1;
			}
		}
		
		return pos;
	}
	
	public void add(T element) {
		int pos = 0;
		int inici = 0;
		int fi = llista.size() - 1;
		int mig;
		T elementMig;
		int comparacio;
		while (fi - inici >= 0) {
			mig = (inici + fi) / 2;
			elementMig = llista.get(mig);
			comparacio = element.compareTo(elementMig);
			if (comparacio == 0) {
				inici = fi + 1;
				pos = mig;
			} else if (comparacio < 0) {
				fi = mig - 1;
				pos = mig;
			} else { // element > elementMig
				pos = inici = mig + 1;
			}
			
		}
		llista.add(pos, element);
	}
	
	public T get(int index) {
		return llista.get(index);
	}
	
	public boolean remove(T element) {
		int pos = binarySearch(element);
		if (pos == -1)
			return false;
		else {
			llista.remove(pos);
			return true;
		}
	}
	
	public int size() {
		return llista.size();
	}
}
