package jardi;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Scanner;

/**
 * A la classe Principal creem un jardí, hi posem alguna planta,
 * i fem passar un torn cada cop que l'usuari introdueix una línia
 * de text.
 */
public class Principal {
	/**
	 * El jardí que es crea, de mida 40.
	 */
	private Jardi jardi;

	/**
	 * El main es limita a crear un objecte de tipus Principal. El
	 * constructor d'aquest objecte fa la resta. Això és habitual, com
	 * que treballem amb un objecte, evita haver de declarar la resta de
	 * propietats i mètodes de la classe com static.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		new Principal();
	}

	/**
	 * S'inicialitza el jardí, i es demana entrada a l'usuari. Cada línia que
	 * s'introdueix avança un torn el jardí. Si l'usuari introdueix la cadena
	 * "surt" s'acaba el programa.
	 */
	public Principal() {
		Scanner sc = new Scanner(System.in);
		String ordre = "";
		
		// Comprova si tenim un jardí guardat. Si no, crea un jardí inicial.
		if (!llegeix()) {
			jardi = new Jardi(40);
			jardi.posaElement(new Altibus(), 10);
			jardi.posaElement(new Declinus(), 30);
		}
		
		while (!ordre.equals("surt")) {
			jardi.temps();
			System.out.println(jardi.toString());
			ordre = sc.nextLine();
		}
		// Guardem el jardí abans de sortir
		guarda();
		sc.close();
	}
	
	/**
	 * Recupera un jardí guardat al fitxer "jardi.sav".
	 * 
	 * @return  true si s'ha pogut llegir el fitxer, false si no existeix
	 * el fitxer, o hi ha hagut algun error.
	 */
	private boolean llegeix() {
		boolean resultat = false;
		if (Files.isRegularFile(Paths.get("jardi.sav"))) {
			try (ObjectInputStream lector = new ObjectInputStream(
					new FileInputStream("jardi.sav"))) {
				jardi = (Jardi) lector.readObject();
				resultat = true;
			} catch (IOException | ClassNotFoundException e) {
				System.err.println(e.getMessage());
			}
		}
		return resultat;
	}

	/**
	 * Guarda el jardí al fitxer "jardi.sav".
	 */
	private void guarda() {
		try (ObjectOutputStream escriptor = new ObjectOutputStream(
				new FileOutputStream("jardi.sav"))) {
			escriptor.writeObject(jardi);
		} catch (IOException e) {
			System.err.println(e.getMessage());
		}
	}
}
